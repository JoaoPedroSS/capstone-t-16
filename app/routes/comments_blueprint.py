from flask import Blueprint
from app.controllers.comments_controller import create_comment_for_item, delete_comment_for_item, get_comments_by_item, update_comment

bp_comments = Blueprint('bp_comments', __name__, url_prefix='/items')

bp_comments.post('/<int:id>/comments')(create_comment_for_item)
bp_comments.get('/<int:id>/comments')(get_comments_by_item)
bp_comments.patch('/comments/<int:id>')(update_comment)
bp_comments.delete('/comments/<int:id>')(delete_comment_for_item)


