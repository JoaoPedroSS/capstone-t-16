from flask import Blueprint
from app.controllers.favorites_controller import add_favorites, remove_favorites, all_favorites

bp_favorite = Blueprint('bp_favorite', __name__, url_prefix='/favorites')


bp_favorite.post('/<int:id_item>')(add_favorites)
bp_favorite.delete('/<int:id_item>')(remove_favorites)
bp_favorite.get('')(all_favorites)
