from flask import Blueprint
from app.controllers.item_controller import (
    create_item, get_item_by_id, update_item, 
    delete_item, get_all_items, get_logged_user_items, 
    get_donated_items, 
)
from app.controllers.tag_controller import get_tag, get_all_tags

bp_item = Blueprint('bp_item', __name__, url_prefix='/items')

bp_item.post('')(create_item)
bp_item.get('')(get_all_items)
bp_item.get('/<int:id>')(get_item_by_id)
bp_item.patch('/<int:id>')(update_item)
bp_item.delete('/<int:id>')(delete_item)
bp_item.get('/user')(get_logged_user_items)
bp_item.get('/tag')(get_tag)
bp_item.get('/tags')(get_all_tags)
bp_item.get('/donated')(get_donated_items)
