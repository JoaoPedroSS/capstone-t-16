from flask import Blueprint
from app.controllers.user_controller import create_user, delete_user, get_user, login_user, update_user
from app.routes.favorite_blueprint import bp_favorite


bp_user = Blueprint('bp_user', __name__, url_prefix='/users')

bp_user.post('/signup')(create_user)
bp_user.post('/login')(login_user)
bp_user.get('')(get_user)
bp_user.patch('')(update_user)
bp_user.delete('')(delete_user)

bp_user.register_blueprint(bp_favorite)
