from flask import Flask
from app.routes.user_blueprint import bp_user
from app.routes.item_blueprint import bp_item
from app.routes.comments_blueprint import bp_comments


def init_app(app: Flask):
    app.register_blueprint(bp_user)
    app.register_blueprint(bp_item)
    app.register_blueprint(bp_comments)
