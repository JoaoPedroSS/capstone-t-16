from app.configs.database import db
from sqlalchemy import Column, Integer, String, DateTime
from werkzeug.security import generate_password_hash, check_password_hash
from dataclasses import dataclass
from sqlalchemy.orm import validates, backref
from re import fullmatch
from datetime import datetime
from ipdb import set_trace

from app.exc.UserErrors import EmailAlreadyRegisteredError, InvalidEmailError


@dataclass
class UserModel(db.Model):
    id: int
    name: str
    username: str
    contact: str
    city: str
    uf: str

    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String(127), nullable=False)
    username = Column(String(127), nullable=False, unique=True)
    birthdate = Column(DateTime, nullable=False)
    contact = Column(String(255), nullable=False, unique=True)
    city = Column(String(255), nullable=False)
    uf = Column(String(2), nullable=False)
    email = Column(String(255), nullable=False, unique=True)
    password_hash = Column(String(511), nullable=False)

    comments = db.relationship("CommentsModel", backref=backref("user", uselist=False), uselist=True)

    @validates('name', 'birthdate', 'city', 'uf', 'email')
    def validate_fields(self, key, value):
        if type(value) != str:
            raise TypeError(f'the value {value} is a invalid data in the field {key}!')
        
        if key == 'name' or key == 'city':
            value = value.title()

        elif key == 'birthdate':
            datetime.strptime(value, '%d/%m/%Y')

        elif key == 'uf':
            value = value.upper()

        elif key == 'email':
            regex_email = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'

            if not fullmatch(regex_email, value):
                raise InvalidEmailError(f'{value} is a invalid e-mail!')
            
            registered_user = UserModel.query.filter_by(email=value).first()

            if registered_user:
                raise EmailAlreadyRegisteredError(f'E-mail {value} already registered!')

        return value

    @property
    def password(self):
        raise AttributeError('Password is not acessible!')
    
    @password.setter
    def password(self, password_to_hash):
        self.password_hash = generate_password_hash(password_to_hash)
    
    def check_password(self, password_to_compare):
        return check_password_hash(self.password_hash, password_to_compare)