from sqlalchemy.orm import backref
from app.configs.database import db
from dataclasses import dataclass
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship, backref
from app.exc.ItemsErrors import ExtensionError, KeyNotFound, ValueInvalid
from app.models.item_tag_model import Item_Tag


@dataclass
class ItemModel(db.Model):
    id: int
    name: str
    description: str
    user_review: str
    image: str
    status: str

    __tablename__ = 'items'

    id = Column(Integer, primary_key=True)
    name = Column(String(100), nullable=False)
    description = Column(String(255), nullable=False)
    user_review = Column(String(255), nullable=False)
    image = Column(String, nullable=False)
    status = Column(String, nullable=False, default='Available')
    user_id = Column(Integer, ForeignKey('users.id'))

    user = relationship('UserModel', backref=backref('items', uselist=True), uselist=False)
    tags = relationship('TagModel', backref=backref('items', uselist=True) ,secondary=Item_Tag)
    comments = relationship("CommentsModel", backref=backref("item", uselist=False), uselist=True)

    def validate_expression(filename):
        extension = ['png', 'jpg', 'jpeg']
        ext = filename.split('.')[1]
        if not ext in extension:
            raise ExtensionError('Image format not allowed!')
        
        
    def validate_update(**data):
        all_keys = ["name", "description", "image", "tags", "user_review"]
        for key in data.keys():
            if not key in all_keys:
                raise KeyNotFound(f'Field ({key}) not allowed!')

    
    def validate_keys(**data):
        
        required_key = ["name", "description", "tags", "user_review"]
        
        for key in required_key:
            
            if key not in data:
                raise KeyNotFound(f'the {key} field was not found!')
            
        if type(data["name"]) != str or type(data["description"]) != str:
            raise ValueInvalid('The past value does not match the expected')
