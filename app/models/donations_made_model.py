from app.configs.database import db
from dataclasses import dataclass
from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

@dataclass
class DonationsMade(db.Model):

    item_id: int

    __tablename__ = 'donations'

    id = Column(Integer, primary_key=True)
    item_id = Column(Integer, ForeignKey('items.id'))

    item = relationship('ItemModel', uselist=False)