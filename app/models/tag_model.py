from app.configs.database import db
from sqlalchemy import Column, String, Integer
from dataclasses import dataclass
from sqlalchemy.orm import validates


@dataclass
class TagModel(db.Model):
    name: str

    __tablename__ = 'tags'

    id = Column(Integer, primary_key=True)
    name = Column(String(30), nullable=False, unique=True)


    @validates('name')
    def validate_fields(self, _, value):
        if type(value) != str:
            raise TypeError("The tag name is not data of type string!")

        value = value.lower()

        return value
    
