from sqlalchemy.sql.schema import ForeignKey
from app.configs.database import db
from sqlalchemy import Column, Integer, String
from dataclasses import dataclass
from app.exc.ItemsErrors import KeyNotFound


@dataclass
class CommentsModel(db.Model):
    id: int
    message: str
    like: int
    deslike: int

    __tablename__ = 'comments'

    id = Column(Integer, primary_key=True)
    message = Column(String(127), nullable=False)
    like = Column(Integer, nullable=False, default=0)
    deslike = Column(Integer, nullable=False, default=0)

    user_id = Column( Integer, ForeignKey('users.id'), nullable=False )
    item_id = Column( Integer, ForeignKey('items.id'), nullable=False )

    def validate_update_comments(**data):
        all_keys = ["message", "user_id", "item_id"]
        for key in data.keys():
            if not key in all_keys:
                raise KeyNotFound(f'Field ({key}) not allowed!')

