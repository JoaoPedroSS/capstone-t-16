from sqlalchemy import Column, Integer
from sqlalchemy.sql.schema import ForeignKey
from app.configs.database import db

Item_Tag = db.Table(

    "item_tag",
    Column("id", Integer, primary_key=True),
    Column("item_id", Integer, ForeignKey('items.id')),
    Column("tag_id", Integer, ForeignKey('tags.id'))

)
