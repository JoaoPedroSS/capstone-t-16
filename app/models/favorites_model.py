from sqlalchemy.sql.schema import Column, ForeignKey
from sqlalchemy.sql.sqltypes import Integer, String
from app.configs.database import db
from dataclasses import dataclass
from sqlalchemy import Column, Integer


@dataclass
class FavoriteModel(db.Model):

    __tablename__ = "favorites"

    id = Column(Integer, primary_key=True)
    username = Column(String, nullable=False)
    item_id = Column(Integer, ForeignKey('items.id'))
