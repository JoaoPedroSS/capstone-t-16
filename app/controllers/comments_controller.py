from flask import request, current_app, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity

from app.models.comments_model import CommentsModel
from app.models.item_model import ItemModel
from app.models.user_model import UserModel

from app.exc.CommentsErrors import CommentOwnerError
from app.exc.ItemsErrors import KeyNotFound


@jwt_required()
def create_comment_for_item(id):
    session = current_app.db.session

    data = request.json

    logged_user = get_jwt_identity()

    try:

        user = UserModel.query.get(logged_user['id'])

        item = ItemModel.query.get(id)

        if item == None:
            return {"message": "Item not found!"}, 404

        CommentsModel.validate_update_comments(**data)

        data['user_id'] = user.id
        data['item_id'] = item.id

        new_comment = CommentsModel(**data)

        session.add(new_comment)
        session.commit()

        return jsonify(new_comment), 201

    except (AttributeError, KeyNotFound) as e:
        return {"message": str(e)}, 404


def get_comments_by_item(id):

    try:
        comments = (
            CommentsModel.query
            .filter(CommentsModel.item_id == id)
            .all()
        )

        item = ItemModel.query.get(id)

        serializer = [{
            "item": item.name,
            "comments": [
                {
                    "id": comment.id,
                    "user": comment.user.username,
                    "message": comment.message,
                    "likes": comment.like,
                    "deslikes": comment.deslike,
                }for comment in comments
            ]
        }]

    except AttributeError:
        return {"message": "Item not found!"}, 404
    
    return jsonify(serializer), 200


@jwt_required()
def update_comment(id):
    session = current_app.db.session
    data = request.get_json()

    logged_user = get_jwt_identity()

    try:
        chosen_comment = CommentsModel.query.get(id)

        if len(data) == 0:
            return {'message': 'No data to be updated'}, 400

        if chosen_comment == None:
            return {"message": "Comment not found"}, 404

        if logged_user['id'] == chosen_comment.user.id:
            CommentsModel.validate_update_comments(**data)

            for key, value in data.items():
                if key == 'message':
                    setattr(chosen_comment, key, value)
        else:
            raise CommentOwnerError('Impossible to update, user does not own the comment!')
        
        session.add(chosen_comment)
        session.commit()
    except CommentOwnerError as e:
        return {'message': str(e)}, 400
    except KeyNotFound as e:
        return {'message': str(e)}, 400

    return jsonify(chosen_comment), 200


@jwt_required()
def delete_comment_for_item(id):
    session = current_app.db.session
    logged_user = get_jwt_identity()

    try:
        chosen_comment = CommentsModel.query.get(id)
        if chosen_comment == None:
            return {"message": "Comment not found"}, 404

        if logged_user['id'] == chosen_comment.user.id:
            session.delete(chosen_comment)
            session.commit()
        else:
            raise CommentOwnerError('Impossible to delete, user does not own the comment!')

    except CommentOwnerError as e:
        return {'message': str(e)}, 400

    return "", 204
