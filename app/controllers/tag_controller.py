from flask import request, jsonify
from sqlalchemy.sql.elements import or_
from app.models.tag_model import TagModel
from sqlalchemy import or_


def get_tag():
    tag = request.args.to_dict()
    tags = tag['tags'].replace("-", " ")
    data = request.args.get("tags")
    all_tags = tags.split()

    try:
            
        search_tag = TagModel.query.filter(
            or_ (TagModel.name==tag.lower() for tag in all_tags)).all()  
    
        if len(search_tag) == 0:
            raise IndexError(f'Tag {data} not found!')
        
        items = []

        for tag  in search_tag:
            tag_items = tag.items
            for tag_item in tag_items:
                if tag_item not in items and tag_item.status == "Available":
                    items.append(tag_item)

        result = {
            "tags": {
                "name": [tag.name for tag in search_tag],
                "items": items
            }
        }
        return jsonify(result), 200

    except IndexError as e:
        return {'message': str(e)}, 404
    

def get_all_tags():

    tags = TagModel.query.all()

    return jsonify(tags), 200
