from flask import request, current_app, jsonify
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity
from datetime import datetime
from werkzeug.security import generate_password_hash
from re import fullmatch

from app.controllers.favorites_controller import alterate_username_favorites
from app.models.user_model import UserModel

from sqlalchemy.exc import IntegrityError, InvalidRequestError
from psycopg2.errors import UniqueViolation
from werkzeug.exceptions import NotFound
from app.exc.UserErrors import EmailAlreadyRegisteredError, InvalidEmailError, InvalidUfError, InvalidsFieldsError

USERFIELDS = ['name', 'username', 'birthdate', 'contact', 'city', 'uf', 'email', 'password']

def create_user():
    session = current_app.db.session

    data = request.json

    try:

        for key in USERFIELDS:
            if not key in data:
                raise InvalidsFieldsError(f'the field {key} is missing')

        if len(data['uf']) != 2:
            raise InvalidUfError(f'the uf {data["uf"]} does not have 2 characters. Example: SP')

        user = UserModel(**data)

        session.add(user)
        session.commit()

    except EmailAlreadyRegisteredError as e:
        return {'message': str(e)}, 409

    except InvalidEmailError as e:
        return {'message': str(e)}, 400

    except IntegrityError as e:
        if type(e.orig) == UniqueViolation:
            return {'message': str(e.orig).split('\n')[1]}, 409

    except InvalidsFieldsError as e:
        return {'message': str(e)}, 400
    
    except TypeError as e:
        return {'message': str(e)}, 400

    except InvalidUfError as e:
        return {'message': str(e)}, 400
    
    except ValueError:
        return {'message': 'Invalid date format. Try: DD/MM/YYYY'}, 400

    return jsonify(user), 201


def login_user():
    data = request.json

    try:
        user: UserModel = (
            UserModel.query
            .filter_by(email=data['email'])
            .first_or_404(description='User not found!!')
        )

        if user.check_password(data['password']):
            access_token = create_access_token(user)
            return {'access_token': access_token}, 200
    except NotFound as e:
        return {'message': e.description}, 404
    
    except KeyError as e:
        return {'message': f'the field {e} is missing'}, 400

    return {'message': 'Email and password missmatch!'}, 401


@jwt_required()
def get_user():
    logged_user = get_jwt_identity()
    
    try:
        user = (
            UserModel.query
            .filter_by(id=logged_user['id'])
            .first_or_404(description='User not found!!')
        )
    except NotFound as e:
        return {'message': e.description}, 404

    return jsonify(user), 200


@jwt_required()
def update_user():
    logged_user = get_jwt_identity()

    session = current_app.db.session

    data = request.json

    try:
        if len(data) == 0:
            raise InvalidsFieldsError('No data to be updated')

        treat_data(data)
        
        UserModel.query.get_or_404(logged_user['id'], description='User not found!!')

        user = UserModel.query.filter_by(id=logged_user['id']).first()
        old_username = user.username

        UserModel.query.filter_by(id=logged_user['id']).update(data)
        session.commit()

        alterate_username_favorites(old_username)


    except EmailAlreadyRegisteredError as e:
        return {'message': str(e)}, 409

    except NotFound as e:
        return {'message': e.description}, 404
    
    except InvalidsFieldsError as e:
        return {'message': str(e)}, 400
    
    except InvalidEmailError as e:
        return {'message': str(e)}, 400
    
    except IntegrityError as e:
        if type(e.orig) == UniqueViolation:
            return {'message': str(e.orig).split('\n')[1]}, 409
    
    except ValueError:
        return {'message': 'Invalid date format. Try: DD/MM/YYYY'}, 400

    except InvalidUfError as e:
        return {'message': str(e)}, 400

    except InvalidRequestError as e:
        return {'message': str(e)}, 400

    return {'message': 'user has been updated successfully!!'}, 200


def treat_data(data):

    for key, value in data.items():
        
        if type(value) != str:
            raise TypeError(f'{value} is invalid data!')
            
        if key == 'name' or key == 'city':
            data[key] = data[key].title()

        elif key == 'uf':
            if len(data[key]) != 2:
                raise InvalidUfError(f'the uf {data["uf"]} does not have 2 characters. Example: SP')
            
            data[key] = data[key].upper()

        elif key == 'email':
            regex_email = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'

            if not fullmatch(regex_email, value):
                raise InvalidEmailError(f'{value} is a invalid e-mail!')
            
            registered_user = UserModel.query.filter_by(email=value).first()

            if registered_user:
                raise EmailAlreadyRegisteredError(f'E-mail {value} already registered!')
        elif key == 'birthdate':
            datetime.strptime(value, '%d/%m/%Y')


    if 'password' in data:
        password_to_hash = data.pop('password')

        data['password_hash'] = generate_password_hash(password_to_hash)

    return data


@jwt_required()
def delete_user():
    session = current_app.db.session

    logged_user = get_jwt_identity()

    try:
        user_in_db = (
            UserModel.query
            .filter_by(id=logged_user['id'])
            .first_or_404(description='User not found!!')
        )
        logged_user = user_in_db
        session.delete(user_in_db)

        session.commit()
    except NotFound as e:
        return {'message': e.description}, 404

    return {'message': f'user {logged_user.username} has been deleted successfully!!'}, 200