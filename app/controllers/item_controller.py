from flask import request, current_app, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity

from werkzeug.utils import secure_filename

from app.exc.ItemsErrors import (
    AnotherUserItemError, DataError, DonatedItemError, 
    ExtensionError, ItemNotFoundError, KeyNotFound, 
    ValueInvalid, ValueNotFound
)
from app.models.item_model import ItemModel
from app.models.user_model import UserModel
from app.models.donations_made_model import DonationsMade
from app.models.tag_model import TagModel

import sqlalchemy
from sqlalchemy.orm.exc import UnmappedInstanceError
from werkzeug.exceptions import NotFound
from sqlalchemy import func
import os
import psycopg2
import boto3
from os import getenv

BUCKET_NAME = 'capstone-images-t16'

ACCESS_KEY = getenv("ACCESS_KEY_ID")

s3 = boto3.client(
    's3',
    aws_access_key_id=getenv("ACCESS_KEY_ID"),
    aws_secret_access_key=getenv("ACCESS_SECRET_KEY")
)


@jwt_required()
def create_item():
    session = current_app.db.session

    logged_user = get_jwt_identity()

    try:

        user = UserModel.query.get(logged_user['id'])

        if user == None:
            return {"message": "User not found!"}, 404

        data = {}

        if 'image' not in request.files:
            raise KeyNotFound('You need to add an image of the item')

        for key, value in request.form.items():

            if key == 'tags':
                data[key] = []
                tags = value.split(',')
                for tag in tags:
                    data[key].append({"name": tag.strip()})
            else:
                data[key] = value

        image_received = request.files['image']
        

        filename = secure_filename(image_received.filename)

        if image_received != None:
            ItemModel.validate_expression(filename)
            
        image_received.save(filename)
        s3.upload_file(
            Bucket=BUCKET_NAME,
            Filename=filename,
            Key=filename,
            ExtraArgs={'ACL': 'public-read'}
        )
        
        uri_image = '%s/%s' % ("https://capstone-images-t16.s3.us-east-2.amazonaws.com", filename)

        os.remove(filename)
        data['image'] = str(uri_image)
        ItemModel.validate_keys(**data)
        
        

        data['user_id'] = user.id

        data['name'] = data['name'].title()

        tags = data.pop('tags')
        
        item = ItemModel(**data)
        for tag in tags:
            tag_in_db = TagModel.query.filter(
                TagModel.name == func.lower(tag['name'])).first()

            if tag_in_db != None:
                item.tags.append(tag_in_db)
            else:
                new_tag = TagModel(name=tag['name'])
                item.tags.append(new_tag)

        session.add(item)
        session.commit()

        
        return {
            "item": {
                "id": item.id,
                "name": item.name,
                "description": item.description,
                "user_review": item.user_review,
                "image": uri_image,
                "status": item.status,
                "tags": [tag.name for tag in item.tags]
            }
        }, 200

    except (AttributeError, KeyNotFound, ValueInvalid, ExtensionError) as e:
        return {"message": str(e)}, 400


@jwt_required()
def update_item(id: int):

    session = current_app.db.session

    logged_user = get_jwt_identity()
    
    try:
        data = request.form.to_dict()
        data_image = request.files.get('image')
        if len(data) == 0 and data_image == None:
            raise DataError('no data to be updated')


        ItemModel.validate_update(**data)

        if data_image != None:
            ItemModel.validate_expression(data_image.filename)
        
        if data_image: 
    
            image_received = request.files.get('image')
            filename = secure_filename(image_received.filename)

            image_received.save(filename)
            s3.upload_file(
                Bucket=BUCKET_NAME,
                Filename=filename,
                Key=filename,
                ExtraArgs={'ACL': 'public-read'}
            )
                
            uri_image = '%s/%s' % ("https://capstone-images-t16.s3.us-east-2.amazonaws.com", filename)
            data_image = uri_image
            os.remove(filename)
            
        user = UserModel.query.get(logged_user['id'])
        item = ItemModel.query.get_or_404(id)

        if data_image != None:
            item.image = uri_image
        if item not in user.items:
            raise AnotherUserItemError

        donated = DonationsMade.query.filter(
            DonationsMade.item_id == id).first()

        if donated != None:
            raise DonatedItemError

        if item:
            if data.get('tags') != None:

                item.tags = []
                unformatted_tags = data.get('tags').split(',')
                formated_tags = []
                
                for tag in unformatted_tags:
                    formated_tags.append({"name": tag.strip()})

                for tag in formated_tags:
                    tag_in_db = TagModel.query.filter(
                        TagModel.name == func.lower(tag['name'])).first()

                    if tag_in_db != None:
                        item.tags.append(tag_in_db)
                    else:
                        new_tag = TagModel(name=tag['name'])
                        item.tags.append(new_tag)
                        session.commit()
                        
            if data.get('tags') != None:
                data.pop('tags')
            for item in user.items:
                if item.id == id and len(data) != 0:
                    ItemModel.query.filter(ItemModel.id == id).update(data)
                    session.commit()


            item = ItemModel.query.get(id)
            
            serializer = {
                "id": item.id,
                "name": item.name,
                "description": item.description,
                "user_review": item.user_review,
                "image": item.image,
                "status": item.status,
                "tags": [tag.name for tag in item.tags]
            }

            return jsonify(serializer), 200
        else:
            return {"message": "Item not found!"}, 404

    except (ItemNotFoundError, ValueNotFound, KeyNotFound, ValueInvalid, ExtensionError) as e:
        return {"message": str(e)}, 404
    except DonatedItemError:
        return {"message": "Item was donated!"}, 404
    except AnotherUserItemError:
        return {"message": "You are not the item owner!"}, 400

    except DataError as e:
        return {'message': str(e)}, 400
    
    except NotFound:
        return {'message': 'Item not found!'}, 404


@jwt_required()
def delete_item(id: int):

    session = current_app.db.session

    data = request.json

    logged_user = get_jwt_identity()

    try:

        if data['deletion_reason'].lower() == 'give up':

            user = UserModel.query.get(logged_user['id'])

            item = ItemModel.query.get(id)

            if item not in user.items:
                raise AnotherUserItemError

            if len(user.items) == 0:
                return {"message": "User has no items!"}, 404

            for item in user.items:
                if item.id == id:
                    item = ItemModel.query.filter(
                        ItemModel.id == item.id).first()
                    session.delete(item)

            if not item:
                raise ItemNotFoundError

            session.commit()

            return '', 204

        elif data['deletion_reason'].lower() == 'donated':

            item = ItemModel.query.get(id)

            if not item:
                raise ItemNotFoundError

            user = UserModel.query.get(logged_user['id'])

            if len(user.items) == 0:
                return {"message": "User has no items!"}, 404

            found = False

            for item in user.items:
                if item.id == id:
                    item = ItemModel.query.filter(
                        ItemModel.id == item.id).first()
                    setattr(item, 'status', 'Donated')
                    found = True

            if found != True:
                raise AnotherUserItemError

            session.add(item)
            session.commit()

            verify_already_donated = DonationsMade.query.filter(
                DonationsMade.item_id == id).first()

            if not verify_already_donated:
                item_donated = DonationsMade(item_id=id)

                session.add(item_donated)

                session.commit()

                return jsonify(item_donated.item), 200
            else:
                return {"message": "Item already donated!"}, 400
        else:
            return {"message": "The value must be (give up) or (donated)!"}, 400

    except (UnmappedInstanceError, ItemNotFoundError):
        return {"message": "Item not found!"}, 404
    except KeyError:
        return {"message": '(deletion_reason) key was not passed!'}, 400
    except sqlalchemy.exc.IntegrityError as e:
        if type(e.orig) == psycopg2.errors.ForeignKeyViolation:
            return {"message": "Item was donated!"}, 400
    except AnotherUserItemError:
        return {"message": "You are not the item owner!"}, 400


def get_item_by_id(id: int):

    try:
        item = ItemModel.query.filter(
            ItemModel.id == id, ItemModel.status == 'Available').first()

        return {
            "item": {
                "id": item.id,
                "name": item.name,
                "description": item.description,
                "user_review": item.user_review,
                "image": item.image,
                "status": item.status,
                "tags": [tag.name for tag in item.tags],
                "item_owner": {
                    "username": item.user.username,
                    "contact": item.user.contact,
                    "uf": item.user.uf,
                    "city": item.user.city
                }
            }
        }, 200

    except AttributeError:
        return {"message": "Item not found!"}, 404


def get_all_items():

    items = ItemModel.query.filter(ItemModel.status == 'Available').all()

    return jsonify(items), 200


@jwt_required()
def get_donated_items():
    logged_user = get_jwt_identity()

    user = UserModel.query.get(logged_user['id'])

    items = (
        ItemModel.query
        .filter(ItemModel.user_id == user.id)
        .filter(ItemModel.status == 'Donated')
        .all()
    )
    return jsonify(items), 200


@jwt_required()
def get_logged_user_items():

    logged_user = get_jwt_identity()

    user = UserModel.query.get(logged_user['id'])

    user_items = ItemModel.query.filter(ItemModel.user_id == user.id).all()

    items_list = []

    for item in user_items:

        items_list.append({
            "item": {
                "id": item.id,
                "name": item.name,
                "description": item.description,
                "user_review": item.user_review,
                "image": item.image,
                "status": item.status,
                "tags": [tag.name for tag in item.tags]
            }
        })

    return jsonify(items_list), 200
