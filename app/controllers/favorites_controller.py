from flask import current_app
from app.exc.FavoriteErrors import (
    ItemAlreadyDonatedError, ItemAlreadyFavoritesError, 
    ItemOwnerError, NotInFavoritesError
)
from app.exc.ItemsErrors import ItemNotFoundError
from app.models.item_model import ItemModel
from app.models.user_model import UserModel
from app.models.favorites_model import FavoriteModel
from flask_jwt_extended import jwt_required, get_jwt_identity


@jwt_required()
def add_favorites(id_item: int):
    session = current_app.db.session
    logged_user = get_jwt_identity()
    user = UserModel.query.filter_by(id=logged_user['id']).first()

    try:
        item = ItemModel.query.filter_by(id=id_item).first()

        if item.status == 'Donated':
            raise ItemAlreadyDonatedError

        if not item:
            raise ItemNotFoundError

        if item.user_id == logged_user['id']:
            raise ItemOwnerError

        search_favorite = FavoriteModel.query.filter_by(
            username=user.username, item_id=item.id).one_or_none()

        if search_favorite is not None:
            raise ItemAlreadyFavoritesError

        favorite = FavoriteModel(
            username=user.username, item_id=item.id)

        session.add(favorite)
        session.commit()

        return {"message": f"Item '{item.name}' add yours favorites!"}, 201

    except ItemNotFoundError:
        return {"message": "Item not found!"}, 400
    except ItemAlreadyFavoritesError:
        return {"message": "Item already added to favorites!"}, 409
    except ItemOwnerError:
        return {"message": "You are the owner of the item!"}, 409


@jwt_required()
def remove_favorites(id_item: int):
    session = current_app.db.session
    logged_user = get_jwt_identity()
    user = UserModel.query.filter_by(id=logged_user['id']).first()

    try:
        item = ItemModel.query.filter_by(id=id_item).first()

        if not item:
            raise ItemNotFoundError

        search_favorite = FavoriteModel.query.filter_by(
            username=user.username, item_id=item.id).first()

        if search_favorite is None:
            raise NotInFavoritesError

        session.delete(search_favorite)
        session.commit()

        return {"message": f"Item '{item.name}' remove of yours favorites!"}, 200

    except ItemNotFoundError:
        return {"message": "Item not found!"}, 400
    except ItemAlreadyDonatedError:
        return {"message": "Item has already been donated!"}, 404
    except NotInFavoritesError:
        return {"message": "Item is not in favorites!"}, 404


@jwt_required()
def all_favorites():
    logged_user = get_jwt_identity()
    user = UserModel.query.filter_by(id=logged_user['id']).first()

    favorites = FavoriteModel.query.filter_by(
        username=user.username).all()
    items = []

    for favorite in favorites:
        item = ItemModel.query.filter_by(id=favorite.item_id)
        items.append(list(item)[0])

    return {"favorites": items}, 200

@jwt_required()
def alterate_username_favorites(old_username):
    session = current_app.db.session
    logged_user = get_jwt_identity()
    user = UserModel.query.filter_by(id=logged_user['id']).first()

    favorites = FavoriteModel.query.filter_by(
        username=old_username).all()

    data = {"username": user.username}

    for favorite in favorites:
        FavoriteModel.query.filter(
            FavoriteModel.id == favorite.id).update(data)
        session.commit()
