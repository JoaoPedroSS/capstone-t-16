from flask.app import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def init_app(app: Flask):
    db.init_app(app)
    app.db = db

    from app.models.user_model import UserModel
    from app.models.item_model import ItemModel
    from app.models.tag_model import TagModel
    from app.models.comments_model import CommentsModel
    from app.models.item_tag_model import Item_Tag
    from app.models.favorites_model import FavoriteModel
    from app.models.donations_made_model import DonationsMade
