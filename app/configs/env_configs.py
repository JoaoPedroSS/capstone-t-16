from flask import Flask
from os import getenv, path, mkdir


def init_app(app: Flask):
    app.config['SQLALCHEMY_DATABASE_URI'] = getenv('SQLALCHEMY_DATABASE_URI')
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = bool(getenv('SQLALCHEMY_TRACK_MODIFICATIONS'))
    app.config['JSON_SORT_KEYS'] = False
    app.config['SECRET_KEY'] = getenv('SECRET_KEY')
