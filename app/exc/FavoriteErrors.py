class ItemAlreadyFavoritesError(Exception):
    pass


class NotInFavoritesError(Exception):
    pass


class ItemOwnerError(Exception):
    pass


class ItemAlreadyDonatedError(Exception):
    pass
