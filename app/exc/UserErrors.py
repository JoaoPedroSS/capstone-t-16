class InvalidEmailError(Exception):
    pass

class EmailAlreadyRegisteredError(Exception):
    pass

class InvalidsFieldsError(Exception):
    pass

class InvalidUfError(Exception):
    pass