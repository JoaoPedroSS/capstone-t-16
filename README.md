# Compartilhei API

## Users

### `POST /users/signup`

### Requisição

#

#### Body

![](./doc_img/users/body_create_user.png)

#### Response

- Se tudo der certo deverá retornar o usuário criado:

![](./doc_img/users/response_create_user.png)

### `POST /users/login`

### Requisição

#

#### Body

![](./doc_img/users/body_login_user.png)

#### Response

- Se tudo der certo deverá retornar o token de acesso:

![](./doc_img/users/response_login_user.png)

### `GET /users`

### Requisição

#

#### **Autenticação**

- É preciso passar o token de acesso do tipo Bearer para se ter acesso a esta rota.

#### Body

- Não é necessário um corpo da requisição.

#### Response

- Se tudo der certo deverá retornar o usuário que está logado:

![](./doc_img/users/response_get_user.png)

### `PATCH /users`

### Requisição

#

#### **Autenticação**

- É preciso passar o token de acesso do tipo Bearer para se ter acesso a esta rota.

#### Body

- É possível utilizar um ou mais, assim como qualquer campo no corpo para fazer a atualização, exceto o id.

![](./doc_img/users/body_update_user.png)

#### Response

- Se tudo der certo deverá retornar isto:

![](./doc_img/users/response_update_user.png)

### `DELETE /users`

### Requisição

#

#### **Autenticação**

- É preciso passar o token de acesso do tipo Bearer para se ter acesso a esta rota.

#### Body

- Não é necessário um corpo da requisição.

#### Response

- Se tudo der certo deverá retornar o nome do usuário que foi deletado:

![](./doc_img/users/response_delete_user.png)

**============================================================================**

## Items

### `POST /items`

### Requisição

#

#### **Autenticação**

- É preciso passar o token de acesso do tipo Bearer para se ter acesso a esta rota.

#### Body

![](./doc_img/items/body_create_item.png)

#### Response

- Se tudo der certo deverá retornar o item criado:

![](./doc_img/items/response_create_item.png)

### `GET /items`

### Requisição

#

#### Body

- Não é necessário um corpo da requisição.

#### Response

- Se tudo der certo deverá retornar todos os items cadastrados:

![](./doc_img/items/response_get_all_items.png)

### `GET /items/<item_id>`

### Requisição

#

#### Body

- Não é necessário um corpo da requisição.

#### Response

- Se tudo der certo deverá retornar o item de acordo com o id passado:

![](./doc_img/items/response_get_item_by_id.png)

### `GET /items/tag?tags=tag_name-tag_name`

### Requisição

#

#### Body

- Não é necessário um corpo da requisição.

#### Response

- Se tudo der certo deverá retornar todos os itens que contenham uma ou todas as tags passadas.

![](./doc_img/items/response_get_items_by_tag.png)

### `GET /items/tags`

### Requisição

#

#### Body

- Não é necessário um corpo da requisição.

#### Response

- Se tudo der certo deverá retornar todas as tags registradas.

![](./doc_img/items/response_get_all_tags.png)

### `GET /items/user`

### Requisição

#

#### **Autenticação**

- É preciso passar o token de acesso do tipo Bearer para se ter acesso a esta rota.

#### Body

- Não é necessário um corpo da requisição.

#### Response

- Se tudo der certo deverá retornar todos os itens cadastrados do usuário que está logado:

![](./doc_img/items/response_get_items_logged_user.png)

### `GET /items/donated`

### Requisição

#

#### **Autenticação**

- É preciso passar o token de acesso do tipo Bearer para se ter acesso a esta rota.

#### Body

- Não é necessário um corpo da requisição.

#### Response

- Se tudo der certo deverá retornar uma lista com todos os itens do usuário que está logado que já foram doados:

![](./doc_img/items/response_get_items_donated.png)

### `PATCH /items/<item_id>`

### Requisição

#

#### **Autenticação**

- É preciso passar o token de acesso do tipo Bearer para se ter acesso a esta rota.

#### Body

**Atencão**

- As **tags** devem ser passadas separadas por **virgula (,)**.

  ![](./doc_img/items/body_update_item.png)

#### Response

- Se tudo der certo deverá retornar o item atualizado:

![](./doc_img/items/response_update_item.png)

### `DELETE /items/<item_id>`

### Requisição

#

#### **Autenticação**

- É preciso passar o token de acesso do tipo Bearer para se ter acesso a esta rota.

#### Body

- O usuário pode escolher uma das seguintes opções quando for deletar um item:
  - `"donated"` => Irá excluir o item de todos os itens e adicionar nos itens doados do usuário.
  - `"give up"` => Irá excluir o item de todos os itens

![](./doc_img/items/body_delete_item_1.png)
![](./doc_img/items/body_delete_item_2.png)

#### Response

- Se tudo der certo deverá retornar:
  - O item com o status atualizado para `"Donated"` quando for escolhido `"donated"` no corpo da requisição.
  - Uma resposta vazia com o status code 204 quando for escolhido `"give up"` no corpo da requisição.

![](./doc_img/items/response_delete_item_1.png)
![](./doc_img/items/response_delete_item_2.png)

**============================================================================**

## Comments

### `POST /items/<item_id>/comments`

### Requisição

#

#### **Autenticação**

- É preciso passar o token de acesso do tipo Bearer para se ter acesso a esta rota.

#### Body

![](./doc_img/comments/body_add_comment.png)

#### Response

- Se tudo der certo deverá retornar o comentário feito.

![](./doc_img/comments/response_add_comment.png)

### `GET /items/<item_id>/comments`

### Requisição

#

#### Body

- Não é necessário um corpo da requisição.

#### Response

- Se tudo der certo deverá retornar todos os comentários feito de um determinado item.

![](./doc_img/comments/response_get_all_comments_by_item.png)

### `PATCH /items/comments/<comment_id>`

### Requisição

#

#### **Autenticação**

- É preciso passar o token de acesso do tipo Bearer para se ter acesso a esta rota.

#### Body

- Só será aceito o campo `"message"` no corpo.

![](./doc_img/comments/body_update_comment.png)

#### Response

- Se tudo der certo deverá retornar o comentário com o dado atualizado.

![](./doc_img/comments/response_update_comment.png)

### `DELETE /items/comments/<comment_id>`

### Requisição

#

#### **Autenticação**

- É preciso passar o token de acesso do tipo Bearer para se ter acesso a esta rota.

#### Body

- Não é necessário um corpo da requisição.

#### Response

- Se tudo der certo deverá retornar uma resposta vaiza com o status code 204.

![](./doc_img/comments/response_delete_comment.png)

**============================================================================**

## Favorites

### `POST /users/favorites/<item_id>`

### Requisição

#

#### **Autenticação**

- É preciso passar o token de acesso do tipo Bearer para se ter acesso a esta rota.

#### Body

- Não é necessário um corpo da requisição.

#### Response

- Se tudo der certo deverá retornar uma mensagem com o item que foi adicionado.

![](./doc_img/favorites/response_add_favorites.png)

### `GET /users/favorites`

### Requisição

#

#### **Autenticação**

- É preciso passar o token de acesso do tipo Bearer para se ter acesso a esta rota.

#### Body

- Não é necessário um corpo da requisição.

#### Response

- Se tudo der certo deverá retornar uma lista com todos os favoritos do usuário logado.

![](./doc_img/favorites/response_get_all_favorites.png)

### `DELETE /users/favorites/<item_id>`

### Requisição

#

#### **Autenticação**

- É preciso passar o token de acesso do tipo Bearer para se ter acesso a esta rota.

#### Body

- Não é necessário um corpo da requisição.

#### Response

- Se tudo der certo deverá retornar uma mensagem com o item que foi removido dos favoritos.

![](./doc_img/favorites/response_delete_favorite.png)
