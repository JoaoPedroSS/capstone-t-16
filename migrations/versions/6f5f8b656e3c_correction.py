"""correction

Revision ID: 6f5f8b656e3c
Revises: af2d904dc2c8
Create Date: 2021-12-13 12:43:34.687764

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6f5f8b656e3c'
down_revision = 'af2d904dc2c8'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('comments', 'like',
               existing_type=sa.INTEGER(),
               nullable=False)
    op.alter_column('comments', 'deslike',
               existing_type=sa.INTEGER(),
               nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('comments', 'deslike',
               existing_type=sa.INTEGER(),
               nullable=True)
    op.alter_column('comments', 'like',
               existing_type=sa.INTEGER(),
               nullable=True)
    # ### end Alembic commands ###
